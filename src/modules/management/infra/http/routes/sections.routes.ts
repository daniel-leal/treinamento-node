import { Router } from 'express';
import { getCustomRepository } from 'typeorm';

import SectionsRepository from '@modules/management/repositories/SectionsRepository';

import CreateSectionService from '@modules/management/services/CreateSectionService';
import UpdateSectionService from '@modules/management/services/UpdateSectionService';

const sectionsRouter = Router();

sectionsRouter.get('/', async (request, response) => {
  try {
    const { description } = request.query;
    console.log(request.query);

    const sectionsRepository = getCustomRepository(SectionsRepository);

    const section = await sectionsRepository.findByDescription(
      description?.toString(),
    );

    return response.json(section);
  } catch (error) {
    console.log(error);
    return response.status(400).json(error);
  }
});

sectionsRouter.post('/', async (request, response) => {
  try {
    const { id, description, number, state } = request.body;

    const createSection = new CreateSectionService();

    const section = await createSection.execute({
      description,
      id,
      number,
      state,
    });

    return response.status(201).json({ data: section });
  } catch (error) {
    return response.status(400).json({ error: error.message });
  }
});

sectionsRouter.put('/:id', async (request, response) => {
  try {
    const { id } = request.params;
    const { description, number, state, active } = request.body;

    const updateSection = new UpdateSectionService();

    const section = await updateSection.execute({
      description,
      id,
      number,
      state,
      active,
    });

    return response.json({ data: section });
  } catch (error) {
    return response.status(400).json({ error: error.message });
  }
});

sectionsRouter.delete('/:id', async (request, response) => {
  const { id } = request.params;

  const sectionsRepository = getCustomRepository(SectionsRepository);

  const section = await sectionsRepository.findById(id);

  if (!section) {
    response.status(404).json({ message: 'Lotação não encontrada' });
  }

  await sectionsRepository.delete({ id });

  return response.status(204).send();
});

export default sectionsRouter;
