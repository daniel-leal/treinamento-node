import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('sections')
class Section {
  @PrimaryColumn()
  id: string;

  @Column()
  description: string;

  @Column()
  number: string;

  @Column()
  state: string;

  @Column()
  active: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Section;
