import 'reflect-metadata';
import 'dotenv/config';

import express from 'express';
import routes from './routes';

import '../typeorm';

const app = express();

app.use(express.json());
app.use(routes);

app.listen(process.env.PORT, () => {
  console.log(`🚀 Server started on port :${process.env.PORT}`);
});
