import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export default class CreateSection1592831575984 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'sections',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            length: '35',
            isPrimary: true,
            isGenerated: false,
          },
          {
            name: 'description',
            type: 'varchar',
            length: '60',
            isUnique: true,
          },
          {
            name: 'number',
            type: 'varchar',
            length: '8',
          },
          {
            name: 'state',
            type: 'varchar',
            length: '30',
          },
          {
            name: 'active',
            type: 'bool',
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('sections');
  }
}
